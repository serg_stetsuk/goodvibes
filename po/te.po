# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the goodvibes package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: goodvibes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-02-20 22:12+0700\n"
"PO-Revision-Date: 2020-09-01 01:36+0000\n"
"Last-Translator: akash rao <akash.rao.ind@gmail.com>\n"
"Language-Team: Telugu <https://hosted.weblate.org/projects/goodvibes/"
"translations/te/>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.2.1-dev\n"

#: data/io.gitlab.Goodvibes.appdata.xml.in:4
#: data/io.gitlab.Goodvibes.desktop.in:3 src/main.c:149
msgid "Goodvibes"
msgstr "గుడ్ వైబ్స్"

#: data/io.gitlab.Goodvibes.appdata.xml.in:5
#: data/io.gitlab.Goodvibes.desktop.in:5
msgid "Play web radios"
msgstr "వెబ్ రేడియో లని మొదలెట్టండి"

#: data/io.gitlab.Goodvibes.appdata.xml.in:8
msgid "Arnaud Rebillout"
msgstr "ఆర్నాల్డ్ రీబిల్లౌట్"

#: data/io.gitlab.Goodvibes.appdata.xml.in:12
msgid "Goodvibes is a simple internet radio player for GNU/Linux."
msgstr "గుడ్ వైబ్స్ అనునది GNU/Linux లో నడిచే సామాన్యమైన ఇంటర్నెట్ రేడియో ప్లేయరు."

#: data/io.gitlab.Goodvibes.appdata.xml.in:15
msgid ""
"It comes with every basic features you can expect from an audio player, such "
"as multimedia keys, notifications, system sleep inhibition, and MPRIS2 "
"support."
msgstr ""
"ఇది ఆడియో ప్లేయర్ నించి మీరు కోరే మల్టిమీడియా కీస్, నోటిఫికేషన్స్, నిద్రపోనివ్వని సూచికలు, MPRIS2 సపోర్టు వంటి "
"అన్ని మౌలికమైన ఫీచర్ లని కలిగి ఉంది."

#: data/io.gitlab.Goodvibes.desktop.in:4
msgid "Radio Player"
msgstr "రేడియో ప్లేయరు"

#. TRANSLATORS: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/io.gitlab.Goodvibes.desktop.in:7
msgid "Audio;Radio;Player;"
msgstr "ఆడియో;రేడియో;ప్లేయరు;"

#: src/ui/resources/main-window.glade:47 src/ui/gv-main-window.c:222
msgid "No station selected"
msgstr "స్టేషన్ ఎంచుకొనలేదు"

#: src/ui/resources/main-window.glade:63 src/ui/gv-main-window.c:245
msgid "Stopped"
msgstr "ఆపివేయబడినది"

#: src/ui/resources/prefs-window.glade:41
msgid "Quit"
msgstr "బయటకు"

#: src/ui/resources/prefs-window.glade:42
#: src/ui/resources/prefs-window.glade:659
msgid "Close"
msgstr "ముగించుము"

#: src/ui/resources/prefs-window.glade:55
msgid "Close Button"
msgstr "ముగించే బటన్"

#: src/ui/resources/prefs-window.glade:68
msgid "Application"
msgstr "అప్లికేషన్"

#: src/ui/resources/prefs-window.glade:91
msgid "Autoplay on Startup"
msgstr "మొదలైన పిమ్మట ఆటోప్లే"

#: src/ui/resources/prefs-window.glade:104
msgid "Custom Output Pipeline"
msgstr "నచ్చిన ఔట్పుట్ పైప్లైన్"

#: src/ui/resources/prefs-window.glade:135
msgid "Apply"
msgstr "అప్లై"

#: src/ui/resources/prefs-window.glade:158
msgid "Playback"
msgstr "ప్లేబాక్"

#: src/ui/resources/prefs-window.glade:195
msgid "Prevent sleep while playing"
msgstr "ప్లే చేసే సమయంలో నిద్రని ఆపండి"

#: src/ui/resources/prefs-window.glade:208
msgid "System"
msgstr "సిస్టం"

#: src/ui/resources/prefs-window.glade:255
msgid "Native D-Bus Server"
msgstr "ప్రాంతీయ డీ-బస్ సర్వర్"

#: src/ui/resources/prefs-window.glade:267
msgid "MPRIS2 D-Bus Server"
msgstr "MPRIS2 డీ-బస్ సర్వర్"

#: src/ui/resources/prefs-window.glade:280
msgid "D-Bus"
msgstr "డీ-బస్"

#: src/ui/resources/prefs-window.glade:296
msgid "General"
msgstr "సాధారణం"

#: src/ui/resources/prefs-window.glade:321
msgid "Autoset Window Height"
msgstr "కిటికీ ఎత్తుని ఆటోసెట్ చేయండి"

#: src/ui/resources/prefs-window.glade:338
msgid "Theme Variant"
msgstr "థీం రకము"

#: src/ui/resources/prefs-window.glade:351
msgid "System Default"
msgstr "సిస్టం డిఫాల్టు"

#: src/ui/resources/prefs-window.glade:352
msgid "Prefer Dark"
msgstr "ముదురు రంగు ఇష్టం"

#: src/ui/resources/prefs-window.glade:353
msgid "Prefer Light"
msgstr "తేలిక రంగు ఇష్టం"

#: src/ui/resources/prefs-window.glade:367
msgid "Window"
msgstr "కిటికీ"

#: src/ui/resources/prefs-window.glade:394
msgid "Send Notifications"
msgstr "నోటిఫికేషన్స్ పంపుము"

#: src/ui/resources/prefs-window.glade:417
msgid "Notifications"
msgstr "నోటిఫికేషన్స్"

#: src/ui/resources/prefs-window.glade:453
msgid "Console Output"
msgstr "స్క్రీన్ ఔట్పుట్"

#: src/ui/resources/prefs-window.glade:466
msgid "Console"
msgstr "స్క్రీన్"

#: src/ui/resources/prefs-window.glade:485
msgid "Display"
msgstr "డిస్ప్లే"

#: src/ui/resources/prefs-window.glade:524
msgid "Multimedia Hotkeys"
msgstr "మల్టీమీడియా హాట్ కీస్"

#: src/ui/resources/prefs-window.glade:537
msgid "Keyboard"
msgstr "కీబోర్డు"

#: src/ui/resources/prefs-window.glade:564
msgid "Middle Click"
msgstr "మధ్యలోని క్లిక్"

#: src/ui/resources/prefs-window.glade:577
msgid "Play/Stop"
msgstr "ప్లే చేయుము/ఆపుము"

#: src/ui/resources/prefs-window.glade:578
msgid "Mute"
msgstr "మ్యూట్ చేయుము"

#: src/ui/resources/prefs-window.glade:591
msgid "Scrolling"
msgstr "స్క్రోలింగ్"

#: src/ui/resources/prefs-window.glade:604
msgid "Change Station"
msgstr "స్టేషన్ ని మార్చుము"

#: src/ui/resources/prefs-window.glade:605
msgid "Change Volume"
msgstr "వోల్యూం ని మార్చుము"

#: src/ui/resources/prefs-window.glade:619
msgid "Mouse (Status Icon Mode)"
msgstr "మౌస్ (స్టాటస్ ఐకన్ మోడ్)"

#: src/ui/resources/prefs-window.glade:638
msgid "Controls"
msgstr "కంట్రోల్స్"

#: src/ui/resources/station-dialog.glade:13
#: src/ui/resources/station-properties-box.glade:34
msgid "Name"
msgstr "పేరు"

#: src/ui/resources/station-dialog.glade:36
#: src/ui/resources/station-properties-box.glade:46
msgid "URI"
msgstr "యూ ఆర్ ఐ"

#: src/ui/resources/station-dialog.glade:63
msgid "Security Exception"
msgstr ""

#: src/ui/resources/station-dialog.glade:74
#, fuzzy
msgid "Remove"
msgstr "స్టేషన్ ని తొలగించండి"

#: src/ui/resources/station-properties-box.glade:14
msgid "Station"
msgstr "స్టేషన్"

#: src/ui/resources/station-properties-box.glade:59
msgid "Streams"
msgstr "స్ట్రీం లు"

#: src/ui/resources/station-properties-box.glade:71
msgid "User-Agent"
msgstr "యూజర్ - ఏజెంట్"

#: src/ui/resources/station-properties-box.glade:83
msgid "Codec"
msgstr "కోడెక్"

#: src/ui/resources/station-properties-box.glade:95
msgid "Channels"
msgstr "చానెల్ లు"

#: src/ui/resources/station-properties-box.glade:107
msgid "Sample Rate"
msgstr "నమూనా రేటు"

#: src/ui/resources/station-properties-box.glade:119
msgid "Bitrate"
msgstr "బిట్ రేటు"

#: src/ui/resources/station-properties-box.glade:227
msgid "Metadata"
msgstr "మెటా డేటా"

#: src/ui/resources/station-properties-box.glade:247
msgid "Title"
msgstr "నామధేయం"

#: src/ui/resources/station-properties-box.glade:259
msgid "Artist"
msgstr "కళాకారుడు"

#: src/ui/resources/station-properties-box.glade:271
msgid "Album"
msgstr "ఆల్బం"

#: src/ui/resources/station-properties-box.glade:283
msgid "Genre"
msgstr "రకము"

#: src/ui/resources/station-properties-box.glade:295
msgid "Year"
msgstr "సంవత్సరం"

#: src/ui/resources/station-properties-box.glade:307
msgid "Comment"
msgstr "కామెంట్"

#: src/core/gv-engine.c:213 src/core/gv-station-list.c:1283
#, c-format
msgid "%s: %s"
msgstr "%s: %s"

#: src/core/gv-engine.c:214
msgid "Failed to parse pipeline description"
msgstr "పైప్ లైన్ వర్ణనని పార్స్ చేయలేకున్నాము"

#: src/core/gv-player.c:843
#, c-format
msgid "'%s' is neither a known station or a valid URI"
msgstr "'%s' తెలిసిన స్టేషన్ కానీ తగిన URI కానీ కాదు"

#: src/core/gv-station-list.c:1284
msgid "Failed to save station list"
msgstr "స్టేషన్ లిస్టు సేవ్ చేయలేకున్నాము"

#: src/ui/gv-main-window.c:235 src/ui/gv-main-window.c:263
#: src/feat/gv-inhibitor.c:142 src/feat/gv-notifications.c:68
#: src/feat/gv-notifications.c:113
msgid "Playing"
msgstr "నడుస్తోంది"

#: src/ui/gv-main-window.c:238
msgid "Connecting…"
msgstr "సంధానమవుతోంది…"

#: src/ui/gv-main-window.c:241
msgid "Buffering…"
msgstr "బఫర్ అవుతోంది…"

#: src/ui/gv-main-window.c:420
msgid "Add a security exception?"
msgstr ""

#: src/ui/gv-main-window.c:424
#, c-format
msgid "An error happened while trying to play %s."
msgstr ""

#: src/ui/gv-main-window.c:427 src/ui/gv-station-dialog.c:298
msgid "Cancel"
msgstr "రద్దు చేయుము"

#: src/ui/gv-main-window.c:428
msgid "Add"
msgstr ""

#: src/ui/gv-main-window.c:441
#, fuzzy
msgid "URL"
msgstr "యూ ఆర్ ఐ"

#: src/ui/gv-main-window.c:449 src/feat/gv-notifications.c:125
msgid "Error"
msgstr "తప్పు"

#: src/ui/gv-prefs-window.c:284
msgid "Feature disabled at compile-time."
msgstr "కంపైల్ సమయంలో ఫీచర్ నిరుపయోగం చేయబడినది."

#: src/ui/gv-prefs-window.c:431
msgid "Action when the close button is clicked."
msgstr "ఆపివేయు బటన్ నొక్కినపుడు చేసే పని."

#: src/ui/gv-prefs-window.c:437 src/ui/gv-prefs-window.c:500
msgid "Setting not available in status icon mode."
msgstr "స్టాటస్ ఐకన్ పద్ధతిలో సెట్టింగ్ లభించలేదు."

#: src/ui/gv-prefs-window.c:441
msgid "Whether to start playback automatically on startup."
msgstr "మొదలవగానే ప్లేబాక్ ని మొదలెట్టాలా."

#: src/ui/gv-prefs-window.c:447
msgid "Whether to use a custom output pipeline."
msgstr "నచ్చిన ఔట్పుట్ పైప్ లైన్ ని వాడాలా."

#: src/ui/gv-prefs-window.c:453
msgid ""
"The GStreamer output pipeline used for playback. Refer to the online "
"documentation for examples."
msgstr "ప్లేబాక్ కోసం GStreamer ఔట్పుట్ పైప్ లైన్. ఉదాహరణల కొరకు ఆన్లైన్ సమాచారాన్ని సంప్రదించండి."

#: src/ui/gv-prefs-window.c:469
msgid "Prevent the system from going to sleep while playing."
msgstr "నడుచు సమయమున సిస్టం ని నిద్రపో నివ్వకండి."

#: src/ui/gv-prefs-window.c:474
msgid "Enable the native D-Bus server (needed for the command-line interface)."
msgstr "ప్రాంతీయ డీ-బస్ సర్వర్ ని మేల్కొలిపి ఉంచండి (కమాండ్-లైన్ ఇంటర్ఫేచ్ కోసం అవసరం)."

#: src/ui/gv-prefs-window.c:480
msgid "Enable the MPRIS2 D-Bus server."
msgstr "MPRIS2 డీ-బస్ సర్వర్ ని మేల్కొలపండి."

#. Display
#: src/ui/gv-prefs-window.c:486
msgid "Prefer a different variant of the theme (if available)."
msgstr "థీం వేరే రకం నచ్చింది (లభ్యమైతే)."

#: src/ui/gv-prefs-window.c:493
msgid ""
"Automatically adjust the window height when a station is added or removed."
msgstr "ఒక స్టేషన్ ని చేర్చినపుడు లేక తొలగించినపుడు కిటికీ ఎత్తును ఆటోమేటిగ్గా సరిచేయండి."

#: src/ui/gv-prefs-window.c:504
msgid "Show notification when the status changes."
msgstr "స్టాటస్ మారినపుడు నోటిఫికేషన్ ని చూపించండి."

#: src/ui/gv-prefs-window.c:509
msgid "Display information on the standard output."
msgstr "ప్రామాణికమైన ఔట్పుట్ పై సమాచారాన్ని చూపించుము."

#. Controls
#: src/ui/gv-prefs-window.c:515
msgid "Bind mutimedia keys (play/pause/stop/previous/next)."
msgstr "మల్టీమీడియా కీస్ ని బైండ్ చేయండి (ప్లే/ఆగుము/ఆపుము/ముందరి/తరువాతి)."

#: src/ui/gv-prefs-window.c:521
msgid "Action triggered by a middle click on the status icon."
msgstr "స్టాటస్ ఐకన్ పై మధ్య క్లిక్ వలన చర్య మొదలైంది."

#: src/ui/gv-prefs-window.c:527
msgid "Action triggered by mouse-scrolling on the status icon."
msgstr "స్టాటస్ ఐకన్ పై మౌస్ నడపడం వలన చర్య మొదలైంది."

#: src/ui/gv-prefs-window.c:533
msgid "Setting only available in status icon mode."
msgstr "స్టాటస్ ఐకన్ పద్ధతిలోనే సెట్టింగ్ లభ్యం."

#: src/ui/gv-prefs-window.c:648
msgid "Preferences"
msgstr "ఇష్టాలు"

#: src/ui/gv-station-context-menu.c:35 src/ui/gv-station-dialog.c:451
msgid "Add Station"
msgstr "స్టేషన్ ని చేర్చండి"

#: src/ui/gv-station-context-menu.c:36
msgid "Remove Station"
msgstr "స్టేషన్ ని తొలగించండి"

#: src/ui/gv-station-context-menu.c:37 src/ui/gv-station-dialog.c:451
msgid "Edit Station"
msgstr "స్టేషన్ ని ఎడిట్ చేయండి"

#. We don't do it yet
#: src/ui/gv-station-dialog.c:173
msgid "Security Exception removed"
msgstr ""

#: src/ui/gv-station-dialog.c:299
msgid "Save"
msgstr "సేవ్ చేయండి"

#: src/ui/gv-station-properties-box.c:142
msgid "kbps"
msgstr "కేపీబీఎస్"

#: src/ui/gv-station-properties-box.c:144
msgid "unknown"
msgstr "తెలియనిది"

#. TRANSLATORS: we talk about nominal bitrate here.
#: src/ui/gv-station-properties-box.c:148
#: src/ui/gv-station-properties-box.c:158
msgid "nominal"
msgstr "నామమాత్రపు"

#: src/ui/gv-station-properties-box.c:153
#: src/ui/gv-station-properties-box.c:159
msgid "min"
msgstr "అతి తక్కువ"

#: src/ui/gv-station-properties-box.c:154
#: src/ui/gv-station-properties-box.c:160
msgid "max"
msgstr "అతి ఎక్కువ"

#: src/ui/gv-station-properties-box.c:175
msgid "Mono"
msgstr "ఒకేరకం"

#: src/ui/gv-station-properties-box.c:178
msgid "Stereo"
msgstr "స్టీరియో"

#: src/ui/gv-station-properties-box.c:198
msgid "kHz"
msgstr "కిలోహర్జ్"

#: src/ui/gv-status-icon.c:136
msgid "stopped"
msgstr "ఆపివేయబడినది"

#: src/ui/gv-status-icon.c:139
msgid "connecting"
msgstr "సంధానమవుతోంది"

#: src/ui/gv-status-icon.c:142
msgid "buffering"
msgstr "బఫర్ అవుతోంది"

#: src/ui/gv-status-icon.c:145
msgid "playing"
msgstr "ప్లే అవుతోంది"

#: src/ui/gv-status-icon.c:148
msgid "unknown state"
msgstr "తెలియని స్థితి"

#: src/ui/gv-status-icon.c:155
msgid "muted"
msgstr "మ్యూట్ చేయబడినది"

#: src/ui/gv-status-icon.c:159
msgid "vol."
msgstr "వోల్యూం"

#: src/ui/gv-status-icon.c:166
msgid "No station"
msgstr "స్టేషన్లు లేవు"

#: src/ui/gv-status-icon.c:173
msgid "No metadata"
msgstr "మెటాడేటా లేదు"

#: src/feat/gv-hotkeys.c:145
#, c-format
msgid ""
"%s:\n"
"%s"
msgstr ""
"%s:\n"
"%s"

#: src/feat/gv-hotkeys.c:146
msgid "Failed to bind the following keys"
msgstr "కింది కీ లని బైండ్ చేయలేము"

#: src/feat/gv-inhibitor.c:121
msgid "Failed to inhibit system sleep"
msgstr "సిస్టం నిద్రని ఆపలేము"

#: src/feat/gv-notifications.c:62
#, c-format
msgid "Playing %s"
msgstr "%s నడుస్తోంది"

#: src/feat/gv-notifications.c:65
#, c-format
msgid "Playing <%s>"
msgstr "<%s> నడుస్తోంది"

#: src/feat/gv-notifications.c:106
msgid "(Unknown title)"
msgstr "(తెలియని నామము)"

#~ msgid "io.gitlab.Goodvibes"
#~ msgstr "io.gitlab.గుడ్వైబ్స్"
